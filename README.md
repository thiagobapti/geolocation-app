# GeoLocation App#

### Running Version ###

[http://thiagobaptista.com.br/apps/geolocation-app/](http://thiagobaptista.com.br/apps/geolocation-app/)

It provides an interface to locate both you and any website on earth, enjoy!

### Resources Used ###
* AngularJS
* Bootstrap
* Google Maps API
* IP API