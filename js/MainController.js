geoLocationApp.controller('MainController', ['$scope','$http', '$timeout', function($scope, $http, $timeout) {

    $scope.working = false;
    $scope.alertMessage = undefined;
    $scope.userData = undefined;
    $scope.webSiteData = undefined;
    $scope.url = '';

    var mapMarkers = [];

    getMyLocation = function(){
        setMessage('Loading data...');
        $scope.working = true;
        $scope.userData = undefined;

        $http({method : "GET", url : "http://ip-api.com/json/"}).then(function mySucces(response) {
                
                setMessage();
                $scope.working = false;

                if(!response.data.status || response.data.status !== 'success'){
                    alert('Error getting my location');
                } else{
                    $scope.userData = response.data;

                    mapAddMarker($scope.userData.lat, $scope.userData.lon, 'My location: ' + 
                        $scope.userData.city + ', ' + $scope.userData.country, true );
                }

        }, function myError(response) {

            setMessage();
            $scope.working = false;
            $scope.userData = undefined;
            alert(response.statusText || 'Unable to access external data');
        });
        
    };

    resetLocation = function(){

        var websiteLocation;

        $scope.userData = undefined;

        for (var i = 0; i < mapMarkers.length; i++) {
            if(mapMarkers[i].userLocation === true){
                mapMarkers[i].setMap(null);
            } else {
                websiteLocation = mapMarkers[i];
            }
        }

        mapMarkers = (websiteLocation ? [websiteLocation] : []);

        mapRedraw();
        $scope.$apply();
    };

    setMessage = function(message){
        $scope.alertMessage = message;
    };

    locateWebsite = function(){

        var apiURL;
        var websiteURL = ($scope.url || '').replace('http://','').replace('https://','');
        if(websiteURL.length < 4 || !websiteURL.indexOf('.')){
            alert('Verify domain or url');
            return;
        }

        setMessage('Loading data...');
        mapRemoveAllMarkers();
        $scope.working = true;
        $scope.userData = undefined;
        $scope.webSiteData = undefined;
        $scope.$apply();

        apiURL = 'http://ip-api.com/json/' + websiteURL;

        $http({method : "GET", url : apiURL}).then(function mySucces(response) {

                setMessage();
                $scope.working = false;

                if(!response.data.status || response.data.status !== 'success'){
                    alert('Error locating ' + response.data.query + ': ' + response.data.message);
                } else{
                    $scope.webSiteData = response.data;

                    mapAddMarker($scope.webSiteData.lat, $scope.webSiteData.lon, $scope.url + ' location: ' +
                    $scope.webSiteData.city + ', ' + $scope.webSiteData.country + ' - Lat:' + $scope.webSiteData.lat +
                    ' Lng' + $scope.webSiteData.lon, false);
                }
            }, function myError(response) {

                setMessage();
                $scope.working = false;
                alert(response.statusText || 'Unable to access external data');
            }
        );

    };

    mapAddMarker = function(lat, long, label, userLocation){

        var marker = new google.maps.Marker({
            position: {
                'lat': lat,
                'lng': long
            },
            map: map,
            title: label
        });

        marker.userLocation = userLocation;
        mapMarkers.push(marker);
        mapRedraw();
    };

    mapRedraw = function(){

        $timeout(function(){

            google.maps.event.trigger(map, 'resize');

            var bounds = new google.maps.LatLngBounds();
                for (var i = 0; i < mapMarkers.length; i++) {
                bounds.extend(mapMarkers[i].getPosition());
            }

            map.fitBounds(bounds);
        }, 1);
    };

    mapRemoveAllMarkers = function(){

        for (var i = 0; i < mapMarkers.length; i++) {
            mapMarkers[i].setMap(null);
        }

        mapMarkers = [];
    };

    showMyLocationDetails = function(label){
        var now = new Date();
        alert("This is your " + label + " from ISP " + $scope.userData[label] + " at " + now);
    };
}]);